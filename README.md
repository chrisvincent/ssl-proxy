# SSL Proxy Deployment Project

Base project for deploying dockerized SSL proxy using [nginx:alpine](https://hub.docker.com/_/nginx/), [jwilder/docker-gen](https://github.com/jwilder/docker-gen), and [JrCs/LetsEncrypt](https://github.com/JrCs/docker-letsencrypt-nginx-proxy-companion).

Built specifically to take maximum advantage of GitLab CI/CD.

# Requirements

* Fork this project and make any necessary modifications to `scripts/deploy-proxy.sh` and `.gitlab-ci.yml`.
* Add environment variables to GitLab's `Settings -> CI/CD -> Variables` for this project.
